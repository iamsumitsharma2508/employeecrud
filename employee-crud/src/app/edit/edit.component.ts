import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../model/employee';
import { EmployeeService } from '../services/EmployeeService';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  public employee: Employee;
  public onClickValidation: boolean;
  recordId:string; 

  constructor(protected route: ActivatedRoute,private employeeService: EmployeeService,protected router: Router) { 
    this.employee = new Employee();
    this.onClickValidation = false;
  }

  ngOnInit() {
    this.recordId = this.route.snapshot.paramMap.get("id");
    this.employeeService.fetchEmployeeById(this.recordId).then(resp=> {
      this.employee = resp;
    });
  }

  async update(form) {
    if(!form.valid) {
      this.onClickValidation = true;
      return;
    }
    await this.employeeService.updateEmployee(this.employee);
    
  }

}
