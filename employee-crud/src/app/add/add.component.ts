import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../model/employee';
import { EmployeeService } from '../services/EmployeeService';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  public employee: Employee;
  public onClickValidation: boolean;

  constructor(private employeeService: EmployeeService,protected router: Router) {
    this.employee = new Employee();
    this.onClickValidation = false;
   }

  ngOnInit() {
  }

  async save(form) {
    if(!form.valid) {
      this.onClickValidation = true;
      return
    }
    await (await this.employeeService.addEmployee(this.employee));
  }

}
