export class Employee {
  public id: number;
  public employee_age: number;
  public employee_name: string;
  public employee_salary: string;
}