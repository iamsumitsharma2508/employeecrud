import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Employee } from "../model/employee";

@Injectable({
    providedIn: 'root'
})

export class EmployeeService {
    constructor(public http: HttpClient,protected router: Router) {
    }

    fetchEmployeeList() {
        const promise = new Promise<Employee[]>(async (resolve, reject) => {
            try {

               const response = this.http.get<any>('http://www.appgrowthcompany.com:5069/api/v1/employee/getAll').subscribe(resp=>{
                if(resp.message == "Success"){
                    resolve(resp.allEmployees);
                    return;
                }
                resolve(resp.allEmployees);
               });
            } catch (error) {

                reject(error);
            }
        });
        return promise;
    }

    fetchEmployeeById(id) {
        const promise = new Promise<Employee>(async (resolve, reject) => {
            try {
               const response = this.http.get<any>(`http://www.appgrowthcompany.com:5069/api/v1/employee/get/${id}`).subscribe(resp=>{
                if(resp.message == "Success"){
                    resolve(resp.data);
                    return;
                }
                resolve(resp.data);
               });
            } catch (error) {

                reject(error);
            }
        });
        return promise;
    }

    async updateEmployee(data: Employee) {
        return this.http.put(`http://www.appgrowthcompany.com:5069/api/v1/employee/update/${data.id}`, data).subscribe(resp=>{
            console.log(resp);
            this.router.navigateByUrl('/');
        });
    }

    async addEmployee(data: Employee) {
        return this.http.post<any>('http://www.appgrowthcompany.com:5069/api/v1/employee/create', data).subscribe(resp=>{
            console.log(resp);
            this.router.navigateByUrl('/');
        });
    }

    deleteEmployee(id) {
        const url = `http://www.appgrowthcompany.com:5069/api/v1/employee/update/${id}`;
        return this.http.delete(url).subscribe(resp=>{
            console.log(resp);
        });
    }
}