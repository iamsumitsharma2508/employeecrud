import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Employee } from '../model/employee';
import { EmployeeService } from '../services/EmployeeService';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public employeeList: Employee[] = [];
  displayedColumns: string[] = ['employee_name', 'employee_age', 'employee_salary','action'];
  dataSource = new MatTableDataSource<Employee>();

  @ViewChild(MatPaginator,{static:false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static:false}) sort: MatSort;

  constructor(private employeeService: EmployeeService) { 
    this.employeeList = new Array<Employee>();
  }

  ngOnInit() {
    this.employeeService.fetchEmployeeList().then(resp=> {
      this.employeeList = resp;
      this.employeeList = this.employeeList.filter(x=> x.id !=null);
      this.dataSource = new MatTableDataSource<Employee>(this.employeeList);
      this.dataSource.paginator = this.paginator;
      console.log(this.paginator,this.sort)
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
